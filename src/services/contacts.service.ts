import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PaginationResponse } from '@datorama/akita';
import { getContacts } from './contacts.data';
import {Contact} from './contact';

@Injectable({ providedIn: 'root' })
export class ContactsService {
  getPage(params): Observable<PaginationResponse<Contact>> {
    return getContacts(params);
  }
}
