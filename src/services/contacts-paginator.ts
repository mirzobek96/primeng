import { inject, InjectionToken} from '@angular/core';
import { PaginatorPlugin } from '@datorama/akita';
import { ContactsQuery } from './contacts.query';

export const CONTACTS_PAGINATOR =
  new InjectionToken('CONTACTS_PAGINATOR', {
    providedIn: 'root',
    factory: () => {
      const contactsQuery = inject(ContactsQuery);
      return new PaginatorPlugin(contactsQuery)
        .withControls()
        .withRange();
    }
  });
