import { Injectable } from '@angular/core';
import { ContactsStore, ContactsState } from './contacts.store';
import { QueryEntity } from '@datorama/akita';

@Injectable({ providedIn: 'root' })
export class ContactsQuery extends QueryEntity<ContactsState> {
  constructor(protected store: ContactsStore) {
    super(store);
  }
}
