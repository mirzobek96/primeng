import {NgModule} from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';

const routes: Routes = [
  {
    path: 'resourceTable',
    loadChildren: () => import('./resource-table/resource-table.module').then(m => m.ResourceTableModule)
  },
  {path: '', redirectTo: 'resourceTable', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
