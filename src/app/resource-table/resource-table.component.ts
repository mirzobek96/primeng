import {Component, Inject, OnInit} from '@angular/core';
import {PaginationResponse, PaginatorPlugin} from '@datorama/akita';
import {CONTACTS_PAGINATOR} from '../../services/contacts-paginator';
import {async, Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {ContactsState} from '../../services/contacts.store';
import {ContactsService} from '../../services/contacts.service';

@Component({
  selector: 'app-resource-table',
  templateUrl: './resource-table.component.html',
  styleUrls: ['./resource-table.component.css']
})
export class ResourceTableComponent implements OnInit {
  pagination$: Observable<PaginationResponse<ContactsState>>;
  perPage = 10;
  search = '';

  constructor(@Inject(CONTACTS_PAGINATOR)
              public paginatorRef: PaginatorPlugin<ContactsState>,
              private contactsService: ContactsService) {
  }

  ngOnInit() {
    this.pagination$ = this.paginatorRef.pageChanges.pipe(
      switchMap((page) => {
        const reqFn = () => this.contactsService.getPage({
          page,
          perPage: this.perPage,
        });
        return this.paginatorRef.getPage(reqFn);
      })
    );

  }

  paginationChange(e) {
    this.perPage = e.rows;
    const reqFn = () => this.contactsService.getPage({
      page: e.page,
      perPage: this.perPage,
    });

    this.pagination$ = this.paginatorRef.getPage(reqFn);
  }

  ngOnDestroy() {
    this.paginatorRef.destroy();
  }

}
