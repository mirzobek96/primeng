import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ResourceTableComponent} from './resource-table.component';
import {ResourceRoutingModule} from './resource-routing.module';
import {AccordionModule} from 'primeng/accordion';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {TableModule} from 'primeng/table';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {PaginatorModule} from 'primeng/paginator';



@NgModule({
  declarations: [ResourceTableComponent],
  imports: [
    CommonModule,
    ResourceRoutingModule,
    FormsModule,
    AccordionModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    HttpClientModule,
    PaginatorModule,
    environment.production ? [] : AkitaNgDevtools.forRoot(),

  ]
})
export class ResourceTableModule { }
